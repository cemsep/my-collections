﻿using System;
using System.Collections.Generic;

namespace MyCollections
{
    class Program
    {
        static void Main(string[] args)
        {
            ChaosArray<int> chaosArrayInt = new ChaosArray<int>();
            ChaosArray<string> chaosArrayString = new ChaosArray<string>();

            void DisplayArrayInt(ChaosArray<int> array)
            {
                Console.WriteLine("[{0}]", string.Join(", ", array.GetAll()));
            }

            void DisplayArrayString(ChaosArray<string> array)
            {
                Console.WriteLine("[{0}]", string.Join(", ", array.GetAll()));
            }

            Console.WriteLine("===== Tryging to set 10 items to both arrays =====");
            Console.WriteLine();

            for (int i = 1; i < chaosArrayInt.GetLength(); i++)
            {
                AddToChaosArray(i*3);
            }

            void AddToChaosArray(int item)
            {
                try
                {
                    chaosArrayInt.Set(item);
                    chaosArrayString.Set($"Item {item}");
                }
                catch (IndexOccupiedException e)
                {
                    Console.WriteLine(" * " + e.Message);
                }
            }
            Console.WriteLine();

            Console.WriteLine("===== Chaos array Int types =====");
            DisplayArrayInt(chaosArrayInt);
            Console.WriteLine();

            Console.WriteLine("===== Chaos array String types =====");
            DisplayArrayString(chaosArrayString);
            Console.WriteLine();

            Console.WriteLine("===== Trying to get 3 random items from both arrays =====");

            for (int i = 0; i < 3; i++)
            {
                GetItemFromChaosArray();
            }

            void GetItemFromChaosArray()
            {
                try
                {
                    Console.WriteLine(" * " + chaosArrayInt.Get());
                    Console.WriteLine(" * " + chaosArrayString.Get());
                }
                catch (IndexNullException e)
                {
                    Console.WriteLine(" * " + e.Message);
                }
            }

        }
    }
}
