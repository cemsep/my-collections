﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MyCollections
{
    class ChaosArray<T> : IEnumerable
    {
        public T[] Array { get; set; }

        public ChaosArray()
        {
            Array = new T[10];
        }

        public IEnumerator GetEnumerator()
        {
            return Array.GetEnumerator();
        }

        public T Get()
        {
            Random random = new Random();
            int randomIndex = random.Next(0, 10);

            if (Array.GetType() == typeof(int))
            {
                if ((int)(object)Array[randomIndex] == 0)
                {
                    throw new IndexNullException(randomIndex);
                }
                else
                {
                    T item = Array[randomIndex];

                    return item;
                }
            } else
            {
                if (Array[randomIndex] == null)
                {
                    throw new IndexNullException(randomIndex);
                }
                else
                {
                    T item = Array[randomIndex];

                    return item;
                }
            }
        }

        public void Set(T item)
        {
            Random random = new Random();
            int randomIndex = random.Next(0, 10);

            if (typeof(T) == typeof(int))
            {
                if ((int)(object)Array[randomIndex] != 0)
                {
                    throw new IndexOccupiedException(randomIndex);
                }
                else
                {
                    Array[randomIndex] = item;
                }
            } else
            {
                if (Array[randomIndex] != null)
                {
                    throw new IndexOccupiedException(randomIndex);
                }
                else
                {
                    Array[randomIndex] = item;
                }
            }
        }

        public int GetLength()
        {
            return Array.Length;
        }

        public T[] GetAll()
        {
            return Array;
        }

    }
}
