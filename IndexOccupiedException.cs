﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyCollections
{
    class IndexOccupiedException : Exception
    {
        public IndexOccupiedException()
        {

        }

        public IndexOccupiedException(int index)
            : base(String.Format($"IndexOccupiedException: The position at index {index} not availbale."))
        {

        }
    }
}
