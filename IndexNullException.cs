﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyCollections
{
    class IndexNullException : Exception
    {
        public IndexNullException()
        {

        }

        public IndexNullException(int index)
            : base(String.Format($"IndexNullException: The element at index {index} is null."))
        {

        }
    }
}
